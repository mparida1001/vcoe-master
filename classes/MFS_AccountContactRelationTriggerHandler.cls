/**
Apex Class Name    : MFS_AccountContactRelationTriggerHandler
Version            : 1.0 
Created Date       : 28 APR 2017
Function           : Handler class that gets called by Account Contact Relation Trigger
when Contacts are created,updated
Modification Log   :
-----------------------------------------------------------------------------
* Developer                   Date                      Description
* ----------------------------------------------------------------------------                 
* Manoj Parida                  04/28/2017              Original Version
* Matt Barth                05/02/2017          Updated calls to utilities and added call to end-date functionality
**/
public with sharing class MFS_AccountContactRelationTriggerHandler
{
    //Handler to update IsActive, StartDate, EndDate on Insert
    public static void beforeInsertAccountContactEventHandler(List<AccountContactRelation> newRelationships)
    {
        Set<Id> uniqueContactIds = new Set<Id>();
        for (AccountContactRelation acr : newRelationships)
        {
            uniqueContactIds.add(acr.ContactId);
        }
        
        Map<Id, List<AccountContactRelation>> existingRels = 
            MFS_AccountContactRelationUtilities.getAllRelationshipsForContacts(uniqueContactIds);

        Map<Id, List<AccountContactRelation>> primaryRelationsByContactId = new Map<Id, List<AccountContactRelation>>();
        
        for (AccountContactRelation acr : newRelationships)
        {
            if (!MFS_AccountContactRelationUtilities.validateNoExistingRelationships(acr, existingRels))
            {
                continue;
            }

            MFS_AccountContactRelationUtilities.syncStandardCustomFieldsBeforeInsert(acr);

            if (acr.IsDirect)
            {
                if (!primaryRelationsByContactId.containsKey(acr.ContactId))
                {
                    primaryRelationsByContactId.put(acr.ContactId, new List<AccountContactRelation>());
                }
                
                primaryRelationsByContactId.get(acr.ContactId).add(acr);
            }
            
            // Remove the end-date from any active relationship
            if (acr.IsActive)
            {
                acr.End_Date__c = acr.EndDate = null;
            }
        }

        if (!primaryRelationsByContactId.isEmpty())
        {
            MFS_AccountContactRelationUtilities.endDateRelationshipsBeforeInsertUpdate(null, primaryRelationsByContactId, existingRels);
        }
    }
    
    //Handler to update IsActive, StartDate, EndDate on update
    public static void beforeUpdateAccountContactEventHandler(Map<Id, AccountContactRelation> oldMap, Map<Id, AccountContactRelation> newMap)
    {
        Map<Id, List<AccountContactRelation>> newPrimaryRelationsByContactId = new Map<Id, List<AccountContactRelation>>();
        Map<Id, List<AccountContactRelation>> oldPrimaryRelationsByContactId = new Map<Id, List<AccountContactRelation>>();
        
        for (Id relationId : newMap.keyset())
        {
            AccountContactRelation acrOld = oldMap.get(relationId);
            AccountContactRelation acrNew = newMap.get(relationId);
            
            MFS_AccountContactRelationUtilities.syncStandardActiveStartEndDateBeforeUpdate(acrOld, acrNew);

            if (acrNew.IsDirect && !acrOld.IsDirect)
            {
                if (!newPrimaryRelationsByContactId.containsKey(acrNew.ContactId))
                {
                    newPrimaryRelationsByContactId.put(acrNew.ContactId, new List<AccountContactRelation>());
                    oldPrimaryRelationsByContactId.put(acrNew.ContactId, new List<AccountContactRelation>());
                }
                
                newPrimaryRelationsByContactId.get(acrNew.ContactId).add(acrNew);
                oldPrimaryRelationsByContactId.get(acrNew.ContactId).add(acrOld);
            }
            else
            {
                MFS_AccountContactRelationUtilities.validatePrimaryFlagNotSetToFalse(acrOld, acrNew);
            }

            /// If the standard primary flag has is different than the custom, and the old custom is the same as the new custom
            /// then the standard has changed and we need to update the custom to match
            if (acrOld.Primary_Relationship__c == acrNew.Primary_Relationship__c && (acrNew.Primary_Relationship__c != acrNew.IsDirect))
            {
                acrNew.Primary_Relationship__c = acrNew.IsDirect;
            }

            // Remove the end-date from any active relationship
            if (acrNew.IsActive)
            {
                acrNew.End_Date__c = acrNew.EndDate = null;
            }
        }
        
        if (!newPrimaryRelationsByContactId.isEmpty())
        {
            Map<Id, List<AccountContactRelation>> existingRels = 
                        MFS_AccountContactRelationUtilities.getAllRelationshipsForContacts(newPrimaryRelationsByContactId.keySet());
            
            MFS_AccountContactRelationUtilities.endDateRelationshipsBeforeInsertUpdate(oldPrimaryRelationsByContactId, 
                                                                                       newPrimaryRelationsByContactId, 
                                                                                       existingRels);
        }
    }

    public static void afterUpdateAccountContactEventHandler(Map<Id, AccountContactRelation> oldMap, Map<Id, AccountContactRelation> newMap)
    {
        Map<Id, AccountContactRelation> primaryRelations = new Map<Id, AccountContactRelation>();
        for(AccountContactRelation newRel : newMap.values())
        {
            if (newRel.Primary_Relationship__c && !newRel.IsDirect)
            {
                primaryRelations.put(newRel.Id, newRel);
            }
        }

        if (!primaryRelations.isEmpty())
        {
            MFS_AccountContactRelationUtilities.updateContactAccountIdAfterRelationshipInsertUpdate(primaryRelations);
        }
        
        MFS_AccountContactRelationUtilities.setEndDateTrueForAllRelationshipsAfterInsertUpdate(newMap);
    }

    public static void afterInsertAccountContactEventHandler(Map<Id, AccountContactRelation> newMap)
    {
        Map<Id, AccountContactRelation> primaryRelations = new Map<Id, AccountContactRelation>();
        for(AccountContactRelation rel : newMap.values())
        {
            if (rel.Primary_Relationship__c && !rel.IsDirect)
            {
                primaryRelations.put(rel.Id, rel);
            }
        }

        if (!primaryRelations.isEmpty())
        {
            MFS_AccountContactRelationUtilities.updateContactAccountIdAfterRelationshipInsertUpdate(primaryRelations);
        }
        
        MFS_AccountContactRelationUtilities.setEndDateTrueForAllRelationshipsAfterInsertUpdate(newMap);
    }
}