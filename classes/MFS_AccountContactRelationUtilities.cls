/**
Apex Class Name    : MFS_AccountContactRelationUtilities
Version            : 1.0 
Created Date       : 28 APR 2017
Function           : Utilities class that gets called by Account Contact Relation Trigger Handler
when Contacts are created,updated
Modification Log   :
-----------------------------------------------------------------------------
* Developer                   Date                   	Description
* ----------------------------------------------------------------------------                 
* Manoj Parida              	04/28/2017              	Original Version
* Matt Barth				05/02/2017			Updated names and added function for end-dating existing relationships
**/
public with sharing class MFS_AccountContactRelationUtilities
{
    
	public static Map<Id, List<AccountContactRelation>> getAllRelationshipsForContacts(Set<Id> contactIds)
    {
        Map<Id, List<AccountContactRelation>> results = new Map<Id, List<AccountContactRelation>>();
        
        for (AccountContactRelation[] allRels : [SELECT Id, AccountId, ContactId, IsActive, Active__c, EndDate, End_Date__c, StartDate, Start_Date__c
											FROM AccountContactRelation
											WHERE ContactId IN : contactIds])
		{
            for (AccountContactRelation acr : allRels)
			{
                if (!results.containsKey(acr.ContactId))
                {
                    results.put(acr.ContactId, new List<AccountContactRelation>());
                }
                
                results.get(acr.ContactId).add(acr);
            }
        }
        
        return results;
    }

    
    public static boolean validateNoExistingRelationships(AccountContactRelation newRelationship, Map<Id, List<AccountContactRelation>> ExistingRelationshipsForContactsById)
    {
        Id contactId = newRelationship.ContactId;
        
        List<AccountContactRelation> relationships = ExistingRelationshipsForContactsById.get(contactId);
        if (null == relationships)
        {
            return true;
        }

        for(AccountContactRelation acr : relationships)
        {
            if (newRelationship.AccountId == acr.AccountId)
            {
                Date dt = (null == newRelationship.StartDate ? newRelationship.Start_Date__c : newRelationship.StartDate);
                if (acr.StartDate == dt)
                {
                    newRelationship.addError('Account-Contact relationship exists already with same start date. Please update existing record.');
                    
                    return false;
                }
            }
        }
        
        return true;
    }

    
    /// Validate that the primary flag was not set to false in the AccountContactRelation
	public static boolean validatePrimaryFlagNotSetToFalse(AccountContactRelation oAcr, AccountContactRelation nAcr)
	{
        if (oAcr.Primary_Relationship__c == true && nAcr.Primary_Relationship__c == false && oAcr.IsDirect == true && nAcr.IsDirect == true)
        {
            /// Cannot set primary flag to false directly, must be the result of isdirect being false
            nAcr.addError('Cannot set relationship to non-primary. Please set new primary relationship.');
            
            return false;
        }
        
        return true;
	}

	//Update IsActive, StartDate, EndDate on Insert
	//The custom fields rule on insert because they will be set only by Informatica, so if they are not null then
	//we know that Informatica purposely set them
	//If the custom fields are not set then copy the standard value into the custom field to sync
	public static void syncStandardCustomFieldsBeforeInsert(AccountContactRelation acr)
	{
        //If custom fields have values then copy custom - > standard
        if (acr.Active__c != null)
        {
            acr.IsActive = acr.Active__c;
        }
        else
        {
            acr.Active__c = acr.IsActive;
        }
        
        if (acr.Start_Date__c == null)
        {
            if (acr.StartDate == null)
            {
                acr.StartDate = acr.Start_Date__c = Date.today();
            }
            else
            {
                acr.Start_Date__c = acr.StartDate;
            }
        }
        else
        {
            acr.StartDate = acr.Start_Date__c;
        }
        
        if (acr.End_Date__c == null)
        {
            acr.End_Date__c = acr.EndDate;
        }
        else
        {
            acr.EndDate = acr.End_Date__c;
        }
        if (acr.IsDirect)
        {
            acr.Primary_Relationship__c = true;
        }
	}

	//Update IsActive, StartDate, EndDate on update
	//Prioritize custom fields - if they have changed then set the standard fields to the custom values
	//Otherwise if the standard fields have changed, then copy them into the custom fields
	public static void syncStandardActiveStartEndDateBeforeUpdate(AccountContactRelation acrOld, AccountContactRelation acrNew)
	{
        //If custom values are changed then copy those to standard(this happens during ETL)
        if(acrNew.Active__c != acrOld.Active__c)
        {
            acrNew.IsActive = acrNew.Active__c;
        }
        else if(acrNew.IsActive != acrOld.IsActive)
        {
            acrNew.Active__c = acrNew.IsActive;
        }
        
        if(acrNew.Start_Date__c != acrOld.Start_Date__c)
        {
            acrNew.StartDate = acrNew.Start_Date__c;
        }
        else if(acrNew.StartDate != acrOld.StartDate)
        {
            acrNew.Start_Date__c = acrNew.StartDate;
        }
        
        if(acrNew.End_Date__c != acrOld.End_Date__c)
        {
            acrNew.EndDate = acrNew.End_Date__c;
        }
        else if(acrNew.EndDate != acrOld.EndDate)
        {
            acrNew.End_Date__c = acrNew.EndDate;
        }
	}

	// Only end-date relationship if isdirect = true and old isdirect = false and end-date flag is checked
	public static void endDateRelationshipsBeforeInsertUpdate(Map<Id, List<AccountContactRelation>> oldRecordsByContactId, 
                                                              Map<Id, List<AccountContactRelation>> newRecordsByContactId,
                                                              Map<Id, List<AccountContactRelation>> AssociatedRelationshipsByContactId)
	{
        /// Contains the records that have relationships that need to have all other relationships end-dated
		Map<Id, AccountContactRelation> newPrimaryWithEndDateCheckedByContactId = new Map<Id, AccountContactRelation>();

		for (Id contactId : newRecordsByContactId.keySet())
        {
			for (AccountContactRelation newRel : newRecordsByContactId.get(contactId))
            {
                /// Do processing if this relationship is the primary relationship.
                if (newRel.IsDirect == true && newRel.End_Date_Relationships__c == true)
                {
                    /// we're
                    boolean newPrimary = false;
    
                    // If this is an insert (no old record) then we know this is a NEW primary relationship                
                    if (null == oldRecordsByContactId || oldRecordsByContactId.isEmpty())
                    {
                        newPrimary = true;
                    }
    
                    /// If this is an Update then we need to see if the old relationship was also primary
                    else
                    {
                        /// loop through the relationships associated with this contactid
                        for (AccountContactRelation oAcr: oldRecordsByContactId.get(newRel.ContactId))
                        {
                            /// If the account matches check to see if it was primary - if it wasn't then we know
                            /// this is a new primary relationship
                            if (oAcr.AccountId == newRel.AccountId)
                            {
                               if (!oAcr.IsDirect)
                               {
                                   newPrimary = true;
                               }
                               
                               break;
                            }
                        }
                    }
                    
                    if (newPrimary)
                    {
                        newPrimaryWithEndDateCheckedByContactId.put(newRel.ContactId, newRel);                    
                    }
                }
            }
		}

        /// If there's no end-dating to do then just get out
		if (newPrimaryWithEndDateCheckedByContactId.isEmpty())
		{
			return;
		}

        /// Collection to hold all the changes
        Map<Id, List<AccountContactRelation>> recordsToSaveByContactId = new Map<Id, List<AccountContactRelation>>();
        
        /// Get all of the relationships associated with contactId's from the new relationships
		for (List<AccountContactRelation> acrList : AssociatedRelationshipsByContactId.values())
		{
            for (AccountContactRelation acr : acrList)
            {
                AccountContactRelation prim = newPrimaryWithEndDateCheckedByContactId.get(acr.ContactId);
                
                /// We'll potentially retrieve the new primary (if it's an update) in the query so
                /// just skip that one. The primary will be null if this isn't an update/insert to a primary 
                if (null == prim || (prim.AccountId == acr.AccountId))
                {
                    continue;
                }
                
                //These guys are already end-dated, just move on
                if (!acr.IsActive && acr.EndDate != null)
                {
                    continue;
                }
                
                // This is not the new primary and it has not been processed in the incoming set
                acr.IsActive = false;
                acr.Active__c = false;
                acr.End_Date__c = Date.Today();
                acr.EndDate = Date.Today();
                
                /// Add to the data set to save
                if (!recordsToSaveByContactId.containsKey(acr.ContactId))
                {
                    recordsToSaveByContactId.put(acr.ContactId, new List<AccountContactRelation>());
                }
                recordsToSaveByContactId.get(acr.ContactId).add(acr);
            }
		}
        
        /// Save the end-dated records
        if (!recordsToSaveByContactId.isEmpty())
        {
            Map<Id, List<DataBase.Error>> errorsByContactId = MFS_DMLUtilities.SaveEntityWithAssociatedId(recordsToSaveByContactId);
            for (Id contactId : errorsByContactId.keySet())
            {
                for (DataBase.Error err : errorsByContactId.get(contactId))
                {
                    newPrimaryWithEndDateCheckedByContactId.get(contactId).addError(err.getMessage());
                }
            }	
        }
	}
    
	public static void updateContactAccountIdAfterRelationshipInsertUpdate(Map<Id, AccountContactRelation> AllRelationships)
	{
		Map<Id, AccountContactRelation> primaryRelsByContactId = new Map<Id, AccountContactRelation>();
		for (AccountContactRelation acr : AllRelationships.values())
		{
			if (acr.Primary_Relationship__c == true && acr.IsDirect == false)
			{
				primaryRelsByContactId.put(acr.ContactId, acr);
			}
		}

		if (primaryRelsByContactId.isEmpty())
		{
			return;
		}

        for (Contact[] contacts : [SELECT Id, AccountId FROM Contact WHERE Id IN : primaryRelsByContactId.keySet()])
		{
			Map<Id, List<Contact>> records = new Map<Id, List<Contact>>();
			
			for (Contact c : contacts)
			{
				AccountContactRelation rel = primaryRelsByContactId.get(c.Id);
				
				c.AccountId = rel.AccountId;
				
				if (!records.containsKey(rel.Id))
				{
					records.put(rel.Id, new List<Contact>());
				}
				records.get(rel.Id).add(c);
			}

			Map<Id, List<DataBase.Error>> errorsByRelationshipId = MFS_DMLUtilities.SaveEntityWithAssociatedId(records);
			for (Id relationshipId : errorsByRelationshipId.keySet())
			{
				for (DataBase.Error err : errorsByRelationshipId.get(relationshipId))
				{
					AllRelationships.get(relationshipId).addError(err.getMessage());
				}
			}
		}
	}

	public static void setEndDateTrueForAllRelationshipsAfterInsertUpdate(Map<Id, AccountContactRelation> AllRelationships)
	{
		Map<Id, AccountContactRelation> relationsToUpdateById = new Map<Id, AccountContactRelation>();
		
		for (AccountContactRelation acr : AllRelationships.values())
		{
			if (acr.End_Date_Relationships__c == false)
			{
				relationsToUpdateById.put(acr.Id, acr);
			}
		}

		if (relationsToUpdateById.isEmpty())
		{
			return;
		}
        
		for (List<AccountContactRelation> targetAcr : [SELECT Id, End_Date_Relationships__c FROM AccountContactRelation WHERE Id IN : relationsToUpdateById.keySet()])
		{
			Map<Id, AccountContactRelation> relsById = new Map<Id, AccountContactRelation>();
			for (AccountContactRelation acr : targetAcr)
			{
				acr.End_Date_Relationships__c = true;
				relsById.put(acr.Id, acr);
			}

			Map<Id, List<DataBase.Error>> errorsByRelationshipId = MFS_DMLUtilities.SaveEntity(targetAcr);
			for (Id relationshipId : errorsByRelationshipId.keySet())
			{
				for (DataBase.Error err : errorsByRelationshipId.get(relationshipId))
				{
					relsById.get(relationshipId).addError(err.getMessage());
				}
			}
		}
	}
}