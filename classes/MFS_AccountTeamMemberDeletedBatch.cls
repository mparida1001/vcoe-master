/***************************************************************************************************
Apex Class Name    : MFS_AccountTeamMemberDeletedBatch 
Version            : 1.0 
Created Date       : 09 October 2017
Function           : Batch class to populate and delete AccountTeamMemberDeleted__c objects.
Modification Log   :
-----------------------------------------------------------------------------
* Developer                   Date					Description
* ----------------------------------------------------------------------------                 
* Alex Gomez				09/10/2017				Original Version
***************************************************************************************************/
global class MFS_AccountTeamMemberDeletedBatch implements Database.Batchable<sObject>, Database.Stateful {
	global string emailBody;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        try {
        	// Get all of the AccountTeamMember objects deleted in the last hour:
			Database.GetDeletedResult r = Database.getDeleted('AccountTeamMember', Datetime.now().addHours(-1), Datetime.now());
			List<Database.DeletedRecord> deletedRecords = r.deletedRecords; 
		
        	System.debug('At MFS_AccountTeamMemberDeletedBatch.Start');
        	System.debug('deletedRecords.size: ' + deletedRecords.size());
            System.debug('deletedRecords.isEmpty: ' + deletedRecords.isEmpty());
            System.debug('deletedRecords != null: ' + deletedRecords != null);
        
        	//Add them to the AccountTeamMemberDeleted__c object:
        	List<AccountTeamMemberDeleted__c> recordsToAdd = new List<AccountTeamMemberDeleted__c>();
			if (deletedRecords.size() > 0) 
      		{ 
         		for (Database.DeletedRecord dr : deletedRecords) 
         		{ 
            		emailBody = emailBody + dr.id + ' was deleted on ' + dr.deletedDate + '\n';
                
                	System.debug('emailBody: ' + emailBody);
                
              		AccountTeamMemberDeleted__c atmd = new AccountTeamMemberDeleted__c();
                	atmd.AccountTeamMemberId__c = dr.id;
                	atmd.DeletedDate__c = dr.deletedDate;
                	recordsToAdd.add(atmd);
         		} 
      		} 
      		else 
      		{ 
        		emailBody = 'No deletions of Account records in '  + 'the last 60 minutes.';
            	System.debug('emailBody: ' + emailBody);
      		} 
        
      		if (recordsToAdd.size() > 0)
      		{
        		database.saveResult[] sr = database.insert(recordsToAdd,false);
      		}
        } //try
        
        catch(Exception ex) {
            System.debug('Error: ' + ex);
            System.debug('emailBody: ' + emailBody);
        } //catch
        
        //Now grab the AccountTeamMemberDeleted objects that have alreads been processed and that we want to delete:
        String query = 'SELECT Id FROM AccountTeamMemberDeleted__c WHERE Processed__c = true';
        return Database.getQueryLocator(query);
        
    } //start
    
    global void execute(Database.BatchableContext BC, List<AccountTeamMemberDeleted__c> accList) {
    	//Process each batch of records:
    	System.debug('In MFS_AccountTeamMemberDeletedBatch.execute');
    	System.debug('accList: ' + accList);        
    	delete accList;
    } //execute
    
	//Implement finish method of Batchable Interface
    global void finish(Database.BatchableContext batchCon)
    {
        System.debug('In MFS_AccountTeamMemberDeletedBatch.finish');
        System.debug('emailBody: ' + emailBody);
        
     	// Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :batchCon.getJobId()];
        
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('MFS_AccountTeamMemberDeletedBatch Apex Job ' + a.Status);
        mail.setPlainTextBody(emailBody);
        
        try {
        	//Just in case Email Deliverablity is turned off, ignore the error:
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        } //try
        
        catch(Exception ex) {
            System.debug('Error: ' + ex);
            System.debug('emailBody: ' + emailBody);
        } //catch

    } //finish
          
} //class