/***************************************************************************************************
Apex Class Name	: MFS_AccountTeamMemberDeletedSchedule 
Version            	: 1.0 
Created Date       	: 10 Oct 2017
Function           	: Scheduler for AccountTeamMemberDeleted__c management.
Modification Log	:
-----------------------------------------------------------------------------
* Developer	Date		Description
* ----------------------------------------------------------------------------                 
* Alex Gomez	10/10/2017	Original Version
***************************************************************************************************/
global with sharing class MFS_AccountTeamMemberDeletedSchedule implements Schedulable{
    global void execute(SchedulableContext sc){
        MFS_AccountTeamMemberDeletedBatch runBatch = new MFS_AccountTeamMemberDeletedBatch();
        Database.executeBatch(runBatch, 1);
    }
}