@isTest(seeAllData = true)
public class MFS_ApexSoapWebServiceTest {
    
    static testMethod void testFeedItemReq(){
        Account acc = MFS_TestDataUtility.createAccount();
        acc.MDM_ID__c = '1234567';
        insert acc;
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = Label.MFS_Owner_User_Id;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        textSegmentInput.text = 'Could you take a look?';
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = acc.Id;
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        MFS_ApexSoapWebService.GetChatterMentions(feedElement.Id);
    }
    
}