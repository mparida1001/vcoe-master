/**
Apex Class Name    : MFS_ContactCoverageTeamTriggerHandler
Version            : 1.0 
Created Date       : 10 October 2017
Function           : Handler Class for Coverage_Team__c to prevent duplicate contacts
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Anthony Mitchell          10/09/2017              Original Version
**/

public class MFS_ContactCoverageTeamTriggerHandler {
 
    public static List<Coverage_Team__c> checkDuplicateCoverageTeamMember(List<Coverage_Team__c> coverageTeamList){
         List<Coverage_Team__c> coverageList = New List<Coverage_Team__c>([select id, name, coverage_owner__c, Coverage_Role__c 
                                                                       from Coverage_Team__c]);                    
            if(coverageList.size() > 0) {
                for(Coverage_Team__c ct : coverageTeamList){
                for (Coverage_Team__c c : coverageList){
                   {        
                    if (c.Coverage_Owner__c == ct.Coverage_Owner__c && c.Coverage_Role__c == ct.Coverage_Role__c)
                      ct.Coverage_Owner__c.addError('Duplicate Coverage Team Member');                                 
    				}              
                }      
            }                           
        }
             return coverageTeamList;
    }
}