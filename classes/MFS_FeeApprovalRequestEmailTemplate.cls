/***************************************************************************************************
Apex Class Name    : MFS_FeeApprovalRequestEmailTemplate
Version            : 1.0 
Created Date       : 25 Oct 2017
Function           : Controller used for all onboarding email templates
Modification Log   :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Manoj Parida            10/25/2017              Original Version
***************************************************************************************************/
global with sharing class MFS_FeeApprovalRequestEmailTemplate {
	
	public Approval_Request__c feeApprRec;
	public String feeAppId{get;set;}
	
	global MFS_FeeApprovalRequestEmailTemplate (){
		
	}
	
	global Approval_Request__c getFeeApprovalRecord(){
		
		feeApprRec = [
		Select Id,
		Opportunity__r.Name,
		Opportunity__r.StageName,
		Opportunity__r.Region__c,
		Opportunity__r.Account.shippingCountry,
		Opportunity__r.Account.Name,
		Opportunity__r.Consultant__c,
		Opportunity__r.Institutional_Tier__c,
		Opportunity__r.Account.Type__c,
		
		from Approval_Request__c
		where id =: feeAppId
		];
	}
    
}