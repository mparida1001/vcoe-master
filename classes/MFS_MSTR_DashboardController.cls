public class MFS_MSTR_DashboardController
{
	public String sf1Tab{get;set;}
	public String classicTab{get;set;}

	public MFS_MSTR_DashboardController(ApexPages.StandardController stdController)
    {

    }
    
    public string getHomePageURL()
    {
        try
        {
            MicroStrategy_URLs__c ob = MicroStrategy_URLs__c.getInstance('HomeDashboard');
            string url = ob.URL__c;
            if (String.isNotBlank(ob.QueryString__c))
            {
                url += ('?' + ob.QueryString__c);
            }
        	return url;
        }
        catch(Exception ex)
        {
            return '';
        }
    }

	public string getTabURL()
    {
        try
        {
            MicroStrategy_URLs__c ob = MicroStrategy_URLs__c.getInstance('BIDashboardWebTab');
            string url = ob.URL__c;
            if (String.isNotBlank(ob.QueryString__c))
            {
                url += ('?' + ob.QueryString__c);
            }
            if(UserInfo.getUiThemeDisplayed() == 'Theme4t' || UserInfo.getUiThemeDisplayed() == 'Theme4d'){
            	url = ob.QueryStringSf1__c;
            }
        	return url;
        }
        catch(Exception ex)
        {
            return '';
        }
    }
    
    public string getRepScorecardURL()
    {
        try
        {
            string mdmId = ApexPages.currentPage().getParameters().get('MDM_Id');
            system.debug('mdmId in getRepScorecardURL :: '+mdmId);
            Id conId;
            Contact con;
            if(String.isBlank(mdmId)){
	            conId = ApexPages.currentPage().getParameters().get('Id');
	            con = [select MDM_Id__c from Contact where id = : conId];
	           	mdmId = con.MDM_ID__c;	
            }
            system.debug('mdmId in getRepScorecardURL 1 :: '+mdmId);
            MicroStrategy_URLs__c ob = MicroStrategy_URLs__c.getInstance('RepScorecard');
            string url = ob.URL__c;
            if (String.isNotBlank(ob.QueryString__c))
            {
            	if(!String.isBlank(mdmId)){
            		url += ('?' + ob.QueryString__c.replace('{!Contact.MDM_ID__c}', mdmId));
            	}else{
            		url += ('?' + ob.QueryString__c.replace('{!Contact.MDM_ID__c}', ''));
            	}
                
            }
            if(UserInfo.getUiThemeDisplayed() == 'Theme4t' || UserInfo.getUiThemeDisplayed() == 'Theme4d') {
                
            	url = ob.QueryStringSf1__c;
                url = url.replace('{!Contact.MDM_ID__c}', mdmId);
            }
        	return url;
        }
        catch(Exception ex)
        {
        	system.debug('Error in Rep URL???'+ex.getStackTraceString()+'\n'+ex.getMessage());
            return '';
        }
    }

    public string getAdvisorSummaryURL()
    {
        try
        {
            string mdmId = ApexPages.currentPage().getParameters().get('MDM_Id');

            Id conId;
            Contact con;
            if(String.isBlank(mdmId)){
	            conId = ApexPages.currentPage().getParameters().get('Id');
	            con = [select MDM_Id__c from Contact where id = : conId];
	           	mdmId = con.MDM_ID__c;	
            }

            MicroStrategy_URLs__c ob = MicroStrategy_URLs__c.getInstance('AdvisorSummary');
            string url = ob.URL__c;
            
            if (String.isNotBlank(ob.QueryString__c))
            {
            	if(!String.isBlank(mdmId)){
            		url += ('?' + ob.QueryString__c.replace('{!Contact.MDM_ID__c}', mdmId));
            	}else{
            		url += ('?' + ob.QueryString__c.replace('{!Contact.MDM_ID__c}', ''));
            	}
                
            }

            if(UserInfo.getUiThemeDisplayed() == 'Theme4t' || UserInfo.getUiThemeDisplayed() == 'Theme4d') {
                
            	url = ob.QueryStringSf1__c;
                url = url.replace('{!Contact.MDM_ID__c}', mdmId);
            }

        	return url;
        }
        catch(Exception ex)
        {
        	system.debug('Error in Advisor Summary - ' + ex.getStackTraceString() + '\n' + ex.getMessage());
            return '';
        }
    }
}