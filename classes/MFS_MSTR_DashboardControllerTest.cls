@isTest(seeAllData = false)
public class MFS_MSTR_DashboardControllerTest {
    
    static testMethod void testDashboardController(){
        List<MicroStrategy_URLs__c> urlList = new List<MicroStrategy_URLs__c>();
        MicroStrategy_URLs__c mstUrlHomeDashboard = new MicroStrategy_URLs__c(name='HomeDashboard',QueryString__c='Server=BOSMSTRAPPS1&Project=Client+Connect&Port=0&evt=2048001&src=Main.aspx.2048001&documentID=E4E4F8B04D7D4B0A0E9BE8AA98ADDD0B&currentViewMedia=1&visMode=0&hiddensections=header,path,dockTop',URL__c='https://mfsbi-s.mfs.com/mstr/asp/Main.aspx');
        MicroStrategy_URLs__c mstUrlBIDashboardWebTab = new MicroStrategy_URLs__c(name='BIDashboardWebTab',QueryString__c='evt=2048001&src=Main.aspx.2048001&documentID=9D7A789B4A201EA84D961AA9F1923275&currentViewMedia=1&visMode=0&Server=BOSMSTRAPPD2&Project=Client%20Connect&Port=34952&share=1&hiddensections=header,path,dockTop',URL__c='http://mfsbi-d/mstr/asp/Main.aspx');
        MicroStrategy_URLs__c mstUrlRepScorecard = new MicroStrategy_URLs__c(name='RepScorecard',QueryString__c='evt=2048001&documentID=50EBD5AF4C3DDB45A6089EA957FB9FAF&Project=Client+Connect&Server=BOSMSTRAPPD1&valuePromptAnswers={!Contact.MDM_ID__c}&hiddensections=header,path,dockTop',URL__c='http://mfsbi-d/mstr/asp/Main.aspx');
        urlList.add(mstUrlHomeDashboard);urlList.add(mstUrlBIDashboardWebTab);urlList.add(mstUrlRepScorecard);
        insert urlList;
        MFS_MSTR_DashboardController mstrController = new MFS_MSTR_DashboardController();
        String url = mstrController.getHomePageURL();
        mstrController.getTabURL();
        mstrController.getRepScorecardURL();
        system.assertNotEquals('', url);
        }
    
    static testMethod void testDashboardControllerWithException(){      
        MFS_MSTR_DashboardController mstrController = new MFS_MSTR_DashboardController();
        String url = mstrController.getHomePageURL();
        mstrController.getTabURL();
        mstrController.getRepScorecardURL();
        system.assertEquals('', url);
    }
    static testMethod void testDashboardExtension(){
        List<MicroStrategy_URLs__c> urlList = new List<MicroStrategy_URLs__c>();
        MicroStrategy_URLs__c mstUrlHomeDashboard = new MicroStrategy_URLs__c(name='HomeDashboard',QueryString__c='Server=BOSMSTRAPPS1&Project=Client+Connect&Port=0&evt=2048001&src=Main.aspx.2048001&documentID=E4E4F8B04D7D4B0A0E9BE8AA98ADDD0B&currentViewMedia=1&visMode=0&hiddensections=header,path,dockTop',URL__c='https://mfsbi-s.mfs.com/mstr/asp/Main.aspx');
        MicroStrategy_URLs__c mstUrlBIDashboardWebTab = new MicroStrategy_URLs__c(name='BIDashboardWebTab',QueryString__c='evt=2048001&src=Main.aspx.2048001&documentID=9D7A789B4A201EA84D961AA9F1923275&currentViewMedia=1&visMode=0&Server=BOSMSTRAPPD2&Project=Client%20Connect&Port=34952&share=1&hiddensections=header,path,dockTop',URL__c='http://mfsbi-d/mstr/asp/Main.aspx');
        MicroStrategy_URLs__c mstUrlRepScorecard = new MicroStrategy_URLs__c(name='RepScorecard',QueryString__c='evt=2048001&documentID=50EBD5AF4C3DDB45A6089EA957FB9FAF&Project=Client+Connect&Server=BOSMSTRAPPD1&valuePromptAnswers={!Contact.MDM_ID__c}&hiddensections=header,path,dockTop',URL__c='http://mfsbi-d/mstr/asp/Main.aspx');
        urlList.add(mstUrlHomeDashboard);urlList.add(mstUrlBIDashboardWebTab);urlList.add(mstUrlRepScorecard);
        insert urlList;
        Account acc = MFS_TestDataUtility.createAccount();
        acc.MDM_ID__c = '1234567';
        insert acc;
        Contact cont1 = MFS_TestDataUtility.createContact(acc.id);
        cont1.MDM_ID__c = '113456';
        insert cont1;
        //MFS_MSTR_DashboardExtension dashboardExtn = new MFS_MSTR_DashboardExtension(new ApexPages.StandardController(cont1));
       // String url = dashboardExtn.getRepScorecardURL();
       // system.assertNotEquals('', url);
        
    }

}