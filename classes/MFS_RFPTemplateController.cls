Global with Sharing class MFS_RFPTemplateController {
    public String PMAPSId {get;set;}
    public PMAPS__c rfpRec;
    public String shareClasses{get;set;}
    public String dueDate{get;set;}
    public String dateReceived{get;set;}
    public String dataAsOfDate{get;set;}
    public String recordLink{get;set;}
    
        
        global MFS_RFPTemplateController(){
            
        }
    
    global PMAPS__c getRFPRec(){
        try{
            rfpRec = [SELECT
                      Id,
                      Name, 
                      Date_Due__c, 
                      Date_Received__c, 
                      Request_Type__c, 
                      Client_Prospect_Name2__r.Name,
                      Opportunity__r.Region__c,
                      Org_Name__r.Type__c,
                      Org_Name__r.Name,
                      Plan_Type__c,  
                      Client_Tier__c,
                      Owner.Name,
                      Sales_Person_Relationship_Manager__r.Name,
                      Strategy__c,
                      Strategy__r.name,
                      Strategy__r.Vehicle__c,
                      Opportunity__r.Probability,
                      lastModifiedDate,
                      Opportunity__r.Amount__c 
                      from PMAPS__c where id = : PMAPSId];  
            getShareClasses(rfpRec);
            getDatesInDDMMYYFormat(rfpRec);
            recordLink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+rfpRec.Id;
        }catch(Exception e){
            System.debug('Exception in MFS_RFPTemplateController - getRFPRec :'+e.getMessage());
        }
        return  rfpRec;
    }
    
    global void getDatesInDDMMYYFormat(PMAPS__c rfpRec){
        if(rfpRec != null && rfpRec.Date_Due__c!= null){
           dueDate =  rfpRec.Date_Due__c.format();
        }
        if(rfpRec != null && rfpRec.Date_Received__c!= null){
           dateReceived =  rfpRec.Date_Due__c.format();
        }
        
           dataAsOfDate =  rfpRec.lastModifiedDate.format('MM/dd/yyyy');
      
        
    }
    
    global void getShareClasses(PMAPS__c rfpRec){
        shareClasses ='';
        List<Share_Classes__c> sClassList;
            if(rfpRec != null  && rfpRec.Strategy__c != null){
                sClassList  = new List<Share_Classes__c>([SELECT Id, Name, Class_Code__c from Share_Classes__c where Product__c =: rfpRec.Strategy__c ]);
            }
        if(sClassList != null && sClassList.size() > 0){
            for(Share_Classes__c shareClass: sClassList){
                shareClasses += shareClasses + shareClass.Name + ', ';
            }
            shareClasses = shareClasses.removeEnd(', ');
        }
        
    }
}