Global class MFS_SendInviteToOutlook {
    public static String startDate{get;set;}
    public static String endDate{get;set;}
    public static String localStartTime{get;set;}
    public static String mStartDate{get;set;}
    public static String mEndDate{get;set;}
    public static String now{get;set;}
    public static String rcCode{get;set;}
    public static Event event{get;set;}
    public static String portfolios{get;set;}
    public static String vehicleTypes{get;set;}
    public static Event_Relation__c eventRel{get;set;}
    public static String formatStr = 'yyyyMMdd\'T\'HHmmss';
    public static String detailURL{get;set;}
    public static String linkText {get; set; }
    
    
    webservice static String sendInvitees(Id eventRelnId){
        System.debug('Id ' + eventRelnId);
        try{
            
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://owaweb2.mfs.com/ews/exchange.asmx');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml; charset=utf-8');
            req.setHeader('Auth', 'Negotiate');
            req.setHeader('User', 'parida_m');
            req.setHeader('Password', 'Itsnov1!');
            // req.SetHeader('SOAPAction','""');
            
            String body = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:m="http://schemas.microsoft.com/exchange/services/2006/messages" xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
            body += '<soapenv:Header><soap:authentication><soap:username>corp/parida_m</soap:username><soap:password>Itsnov1!</soap:password>';
            body += '</soap:authentication><t:RequestServerVersion Version="Exchange2007_SP1" /><t:TimeZoneContext><t:TimeZoneDefinition Id="India Standard Time" />';
            body += '</t:TimeZoneContext></soap:Header>';
            body += '<soap:Body><m:CreateItem SendMeetingInvitations="SendToAllAndSaveCopy"><m:SavedItemFolderId><t:FolderId Id="AAMkADE5Mzg4YzU2LTMxZGEtNGZiMC1iYTI3LThiNzVmNzRhM2M5ZAAuAAAAAADRyRBEkYkgT50EDdecQeDMAQCE49SVxM6hTob3ENnqzM1/AAAAoWOiAAA=" ChangeKey="AgAAABYAAACE49SVxM6hTob3ENnqzM1/AABOVYom" />';
            body += '</m:SavedItemFolderId><m:Items><t:CalendarItem><t:Subject>Speaker Request Meeting</t:Subject><t:Body BodyType="Text">Speaker Request Meeting for Family Funds</t:Body>';
            body +='<t:Importance>Normal</t:Importance><t:Start>2017-10-12T16:30:00.000Z</t:Start><t:End>2017-10-12T17:00:00.000Z</t:End><t:IsAllDayEvent>false</t:IsAllDayEvent>';
            body += '<t:LegacyFreeBusyStatus>OOF</t:LegacyFreeBusyStatus><t:Location>Boston</t:Location><t:RequiredAttendees><t:Attendee><t:Mailbox>';
            body += '<t:EmailAddress>r1ray@mfs.com</t:EmailAddress></t:Mailbox></t:Attendee></t:RequiredAttendees><t:MeetingTimeZone TimeZoneName="India Standard Time" />';
            body += '</t:CalendarItem></m:Items></m:CreateItem></soap:Body></soap:Envelope>';
            
            
            
            
            //<soapenv:Body><tes:hasAccountWithAcctNumb><tes:acctNumb>8688692968</tes:acct//Numb>';
            //body += '</tes:hasAccountWithAcctNumb></soapenv:Body></soapenv:Envelope>';
            
            req.setBody(body);
            
            HTTPResponse res = http.send(req);
            String output = res.getBody();
            System.debug('output: ' + output); 
            return 'Invitees has been sent successfully.';
        }catch(Exception ex){ 
            System.debug('Exception in sendInvitees :: ' + ex.getMessage());
        }
        return 'Invites have not been sent. Please contact administrator.';
    }
    
    /**   webservice static String sendInvitees(Id eventRelnId){
System.debug('Id ' + eventRelnId);
String baseURl = URL.getSalesforceBaseUrl().toExternalForm();

try{
eventRel = [Select Activity_Detail__c,Time_Slot_Start__c, Time_Slot_End__c, Speaker__r.Email, Speaker_Request__r.Call__c, Speaker_Request__r.Location__c,
Speaker__r.Employee_ID__c, Topic__c, Speaker_Request__r.TimeZone__c, Speaker_Request__r.Event_Title__c, Speaker_Request__r.Requester__c,
Speaker_Request__r.Requester__r.Name, Speaker_Request__r.Event_Types__c, Speaker_Request__r.Audience__c, Speaker_Request__r.Dress_Code__c,
Speaker_Request__r.Tier__c, Speaker_Request__r.Asset_Class__c, Speaker_Request__r.Organization__r.Name, Speaker__r.Name,Speaker_Request__r.Organization__r.Type,
Speaker_Request__r.MFS_Contact_for_Presentation_Materials__r.Name, Speaker_Request__r.Link_to_presentation_materials__c,
Speaker_Request__r.Speaker_Expense_RC_Code__c, Speaker_Request__r.Event_Instructions__c, Speaker_Request__r.Call_Note__c, CreatedBy.Name
from Event_Relation__c where Id =: eventRelnId];
now = DateTIme.now().format(formatStr);
OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'R1ray@mfs.com'];
if(eventRel != null){
startDate = eventRel.Time_Slot_Start__c == null ? '':eventRel.Time_Slot_Start__c.format(formatStr);
endDate = eventRel.Time_Slot_End__c==null? '' : eventRel.Time_Slot_End__c.format(formatStr);
String eventTimeZone = (eventRel.Speaker_Request__r.TimeZone__c == null) ? 'EST' : eventRel.Speaker_Request__r.TimeZone__c.subString(1,10).replace('UTC','GMT');
System.debug('eventTimeZone'+eventTimeZone);
localStartTime = eventRel.Time_Slot_Start__c == null ? '':eventRel.Time_Slot_Start__c.format('MM/dd/yyyy hh:mm a',eventTimeZone);
mStartDate = eventRel.Time_Slot_Start__c == null ? '':eventRel.Time_Slot_Start__c.format('MM/dd/yyyy hh:mm a',UserInfo.getTimeZone().getId());
mendDate = eventRel.Time_Slot_End__c == null ? '':eventRel.Time_Slot_End__c.format('MM/dd/yyyy hh:mm a',UserInfo.getTimeZone().getId());
event = new Event();
event = [Select IsAllDayEvent from Event where Activity_Detail__c =:eventRel.Speaker_Request__r.Call_Note__c Limit 1];

List<Event_Relation_Portfolio_Junction__c> portfList = new List<Event_Relation_Portfolio_Junction__c>([select Portfolio__r.Name, Portfolio__r.Vehicle__c from Event_Relation_Portfolio_Junction__c where Event_Relation__c = :eventRelnId]);
portfolios = '';
for(Event_Relation_Portfolio_Junction__c erpj : portfList)
{
if(String.isempty(portfolios))
portfolios = erpj.Portfolio__r.Name;
else
portfolios += ', '+erpj.Portfolio__r.Name;
if(String.isEmpty(vehicleTypes))
vehicleTypes = erpj.Portfolio__r.Vehicle__c;
else
vehicleTypes += ', '+erpj.Portfolio__r.Vehicle__c;
}
User usr = [select RC_Code__c from User where Id = :eventRel.Speaker_Request__r.Requester__c];
rcCode = usr.RC_Code__c;
if(eventRel != null && !String.isEmpty(eventRel.activity_Detail__c)){
Event ev = [select id, recordtype.DeveloperName, WhatId from Event where activity_Detail__c = :eventRel.activity_Detail__c ORDER BY createddate ASC LIMIT 1 ];
if(ev != null && !String.isEmpty(ev.recordtype.DeveloperName)){
linkText = Label.Outlook_Click_Here;
detailURL = baseURl+'/apex/MFS_A360MeetingDetails?id='+eventRel.Activity_Detail__c+'&nooverride=1&returnID='+ev.whatId+'&event=true';
}
}
}
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'R1Ray@mfs.com'};
mail.setToAddresses(toAddresses);
if ( owea.size() > 0 ) {
System.debug('owea.get(0).Id); ' + owea.get(0).Id);
mail.setOrgWideEmailAddressId(owea.get(0).Id);
}
mail.setSubject('Meeting Invitation');
mail.setPlainTextBody('');

Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
attach.filename = 'SendEventToOutlook.ics';
attach.ContentType = 'text/calendar;';
attach.inline = false;
attach.body = invite();
System.debug('attach.body' + attach.body);
mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});
Messaging.SendEmailResult[] er = Messaging.sendEmail(new Messaging.Email[] {mail});
return 'Invitees has been sent successfully.';
}Catch(Exception ex){
System.debug(ex.getMessage()+ex.getLineNUmber());
return 'Invites have not been sent. Please contact administrator.';
}  
return 'Invites have not been sent. Please contact administrator.';
}**/
    
    private static Blob invite() { 
        String txtInvite = '';
        txtInvite += 'BEGIN:VCALENDAR\n';
        txtInvite += 'PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\n';
        txtInvite += 'VERSION:2.0\n';
        txtInvite += 'CALSCALE:GREGORIAN\n';
        txtInvite += 'METHOD:REQUEST\n';
        txtInvite += 'BEGIN:VEVENT\n';
        txtInvite += 'DTSTART;TZID=US-Pacific: ' + startDate + '\n';
        txtInvite += 'DTEND;TZID=US-Pacific: ' + endDate + '\n';
        txtInvite += 'DTSTAMP;TZID=US-Pacific:' + now + '\n';
        txtInvite += 'ORGANIZER;CN=Speaker Request Team' + ':MAILTO:R1ray@mfs.com\n';
        txtInvite += 'UID:4036587160834EA4AE7848CBD028D1D200000000000000000000000000000000\n';
        txtInvite += 'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=\n';
        txtInvite += ' TRUE;X-NUM-GUESTS=0:mailto:mparida@mfs.com\n';
        txtInvite += 'CREATED:' + now+ '\n';
        txtInvite += 'X-ALT-DESC;FMTTYPE="text/html"::<html><body><b>Event Location time Zone : </b>'+eventRel.Speaker_Request__r.TimeZone__c+'<br/><b>Event Local Start Time : </b>';
        txtInvite += localStartTime + '<br/><br/> <b>Event : </b> ' + eventRel.Speaker_Request__r.Event_Title__c + '<br/><b>Requester : </b>' + eventRel.CreatedBy.Name + '<br/><b>Requested on behalf of :</b>';
        txtInvite += eventRel.Speaker_Request__r.Requester__r.Name + '<br/><b>Location : </b>' + eventRel.Speaker_Request__r.Location__c + '<br/><b>Additional Info :</b><br/><b>Meeting Type : </b>';
        txtInvite += eventRel.Speaker_Request__r.Event_Types__c + '<br/><b>Audience : </b>' + eventRel.Speaker_Request__r.Audience__c + '<br/><b>Dress Code : </b>' + eventRel.Speaker_Request__r.Dress_Code__c;
        txtInvite += '<br/><b>Conference Call Info : </b>' + eventRel.Speaker_Request__r.Call__c + '<br/><br/><b>Relationship : </b>' + eventRel.Speaker_Request__r.Organization__r.Name ;
        txtInvite += '<br/><b>Client Tier : </b>' + eventRel.Speaker_Request__r.Tier__c + '<br/><b>Assets with MFS : </b>' + eventRel.Speaker_Request__r.Asset_Class__c;
        txtInvite += 'LAST-MODIFIED:' + now + '\n';
        txtInvite += 'LOCATION:Boston\n';
        txtInvite += 'STATUS:CONFIRMED\n';
        txtInvite += 'SUMMARY:Speaker Request Invite \n';
        txtInvite += 'TRANSP:OPAQUE\n';
        txtInvite += 'END:VEVENT\n';
        txtInvite += 'END:VCALENDAR\n';
        return Blob.valueOf(txtInvite);
    }
    
}